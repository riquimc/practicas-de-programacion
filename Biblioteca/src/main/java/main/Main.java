package main;

import java.util.Scanner;

import servicios.Biblioteca;

public class Main {

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		Biblioteca biblioteca = new Biblioteca();

		System.out.println("Bienvenido/a a la biblioteca G, ¿Qué desea realizar?");
		

		boolean salir = false;
		do {
			System.out.println("1.Dar de alta prestables" + "\n2.Dar de alta copia" + "\n3.Dar de alta usuario"
					+ "\n4.Dar de baja usuario" + "\n5.Conseguir un prestable" + "\n6.Devolver prestable"
					+ "\n7.Listado de prestables" + "\n8.Listado de usuarios" + "\n9.Listado de usuarios multados"
					+ "\n10.Listado de multas y prestables de un usuario" + "\n11.Salir");
			int opcion = sc.nextInt();
			switch (opcion) {
			case 1:
				biblioteca.darAltaPrestables();
				break;
			case 2:
				biblioteca.darAltaCopia();
				break;
			case 3:
				biblioteca.darAltaUsuario();
				break;
			case 4:
				biblioteca.darDeBajaUsuario();
				break;
			case 5:
				biblioteca.darPrestableAUsuario();
				break;
			case 6:
				biblioteca.devolverPrestable();
				break;
			case 7:
				biblioteca.imprimirPrestables();
				break;
			case 8:
				biblioteca.imprimirUsuario();
				break;
			case 9:
				biblioteca.imprimirUsuariosMultados();
				break;
			case 10:
				biblioteca.imprimirUsuario();
				break;
			case 11:
				salir = true;
				break;
			}
		} while (!salir);
	}
}

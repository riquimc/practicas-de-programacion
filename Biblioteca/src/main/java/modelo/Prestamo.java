package modelo;

import java.time.LocalDateTime;

public class Prestamo {
	private Copia copia;
	private LocalDateTime fechaInicio;
	private LocalDateTime fechFin;
	
	

	public Prestamo(LocalDateTime fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public LocalDateTime getFechFin() {
		return fechFin;
	}

	public void setFechFin(LocalDateTime fechFin) {
		this.fechFin = fechFin;
	}

	public LocalDateTime getFechaInicio() {
		return fechaInicio;
	}

	public void setCopia(Copia copia) {
		this.copia = copia;
	}

	public Copia getCopia() {
		return copia;
	}
	
	
	@Override
	public String toString() {
		return "Prestamo [fechaInicio=" + fechaInicio + ", fechFin=" + fechFin + "]";
	}

	public void resetPrestamo() {
		
		this.copia=null;
		
	}

	

}

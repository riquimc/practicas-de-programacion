package modelo;

import java.util.ArrayList;

public class Lector {

	private ArrayList<Prestamo> prestamos;
	private Multa multa;
	private int nSocio;
	private String nombre;
	private String telefono;
	private String direccion;
	private ArrayList<Prestamo> prestamosHistorico;
	private ArrayList<Multa> multasHistorico;

	public Lector(int nSocio, String nombre, String telefono, String direccion) {
		this.nSocio = nSocio;
		this.nombre = nombre;
		this.telefono = telefono;
		this.direccion = direccion;
		this.prestamos = new ArrayList<Prestamo>();
		this.multasHistorico = new ArrayList<Multa>();
		this.prestamosHistorico = new ArrayList<Prestamo>();

	}

	public Multa getMulta() {
		return multa;
	}

	public int getnSocio() {
		return nSocio;
	}

	public String getNombre() {
		return nombre;
	}

	public String getTelefono() {
		return telefono;
	}

	public String getDireccion() {
		return direccion;
	}

	public Prestamo darPrestamo(int i) {
		return prestamos.get(i);
	}
	
	public void añadirMulta(Multa m) {
		this.multa=m;

	}

	public void añadirPrestamos(Prestamo p) {
		prestamos.add(p);

	}

	public int nPrestables() {
		return prestamos.size();
	}
	
	

	public void setPrestamosHistorico(Prestamo prestamosHistorico) {
		this.prestamosHistorico.add(prestamosHistorico);
	}

	public void setMultasHistorico(Multa multasHistorico) {
		this.multasHistorico.add(multasHistorico);
	}

	public void imprimirHistoricos() {
		for(Prestamo p: prestamosHistorico) {
			System.out.println(p.toString());
		}
		
		for(Multa m : multasHistorico) {
			System.out.println(m.toString());
		}
	}
	
	public void devolverCopias() {
		for(Prestamo p: prestamos) {
			p.getCopia().setEstadoCopia(false);
		}
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		result = prime * result + ((telefono == null) ? 0 : telefono.hashCode());
		return result;
	}

	@Override
	public String toString() {
		return "Lector [nSocio=" + nSocio + ", nombre=" + nombre + ", telefono=" + telefono + ", direccion=" + direccion
				+ "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Lector other = (Lector) obj;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		if (telefono == null) {
			if (other.telefono != null)
				return false;
		} else if (!telefono.equals(other.telefono))
			return false;
		return true;
	}

}

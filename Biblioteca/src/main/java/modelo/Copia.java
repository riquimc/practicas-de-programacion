package modelo;

public class Copia {

	private int identificador;
	private boolean estadoCopia;
	private Prestable prestable;

	public Copia(int identificador) {
		this.identificador = identificador;
		this.estadoCopia = false;
	}

	public int getIdentificador() {
		return identificador;
	}

	public boolean getEstadoCopia() {
		return estadoCopia;
	}

	public Prestable getPrestable() {
		return prestable;
	}
	

	public void setEstadoCopia(boolean estadoCopia) {
		this.estadoCopia = estadoCopia;
	}

	public void setPrestable(Prestable prestable) {
		this.prestable = prestable;
	}

	@Override
	public String toString() {
		return "Copia [identificador=" + identificador + ", prestable=" + prestable + "]";
	}
	

	

}

package modelo;

import utiles.Generos;

public class Libro extends Prestable {

	private String autor;
	private Generos genero;
	
	public Libro(String autor, Generos genero, String tejuelo,String titulo) {
		super(tejuelo,titulo);
		this.autor = autor;
		this.genero = genero;
	}

	public String getAutor() {
		return autor;
	}

	public Generos getGenero() {
		return genero;
	}

	@Override
	public String toString() {
		return "Libro [autor=" + autor + ", genero=" + genero + "]";
	}
	

}

package modelo;

import java.util.ArrayList;

public abstract class Prestable {
	private String tejuelo;
	private String titulo;
	private ArrayList<Copia> copias;

	public Prestable(String tejuelo, String titulo) {
		this.tejuelo = tejuelo;
		this.titulo = titulo;
		copias = new ArrayList<Copia>();
	}

	public String getTejuelo() {
		return tejuelo;
	}

	public String getTitulo() {
		return titulo;
	}

	public void setCopias(Copia copia) {
		copias.add(copia);
	}

	public Copia darCopia() {
		boolean salir =false ;
		Copia c=null;
		for (int i =0;i<copias.size() && !salir;i++) {
			if (!copias.get(i).getEstadoCopia()) {
				 c = copias.get(i);
				 salir=true;
			}
		}
		
		
		return c;
	}

	public int nCopias() {
		int tamaño = copias.size();
		return tamaño;

	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tejuelo == null) ? 0 : tejuelo.hashCode());
		result = prime * result + ((titulo == null) ? 0 : titulo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Prestable other = (Prestable) obj;
		if (tejuelo == null) {
			if (other.tejuelo != null)
				return false;
		} else if (!tejuelo.equals(other.tejuelo))
			return false;
		if (titulo == null) {
			if (other.titulo != null)
				return false;
		} else if (!titulo.equals(other.titulo))
			return false;
		return true;
	}

}

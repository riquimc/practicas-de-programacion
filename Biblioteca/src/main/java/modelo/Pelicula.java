package modelo;

public class Pelicula extends Prestable {

	private String minutos;
	private String director;
	private String actorPrincipal;

	public Pelicula(String minutos, String director, String actorPrincipal, String tejuelo, String titulo) {
		super(tejuelo,titulo);
		this.minutos = minutos;
		this.director = director;
		this.actorPrincipal = actorPrincipal;
	}

	public String getMinutos() {
		return minutos;
	}

	public String getDirector() {
		return director;
	}

	public String getActorPrincipal() {
		return actorPrincipal;
	}

	@Override
	public String toString() {
		return "Pelicula [minutos=" + minutos + ", director=" + director + ", actorPrincipal=" + actorPrincipal + "]";
	}
	
	

}

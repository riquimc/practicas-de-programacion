package modelo;

import java.time.LocalDateTime;

public class Multa {

	private LocalDateTime fechaInicio;
	private LocalDateTime fechaFin;
	
	public Multa(LocalDateTime fechaInicio, LocalDateTime fechaFin) {
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
	}

	public LocalDateTime getFechaInicio() {
		return fechaInicio;
	}

	public LocalDateTime getFechaFin() {
		return fechaFin;
	}

	@Override
	public String toString() {
		return "Multa [fechaInicio=" + fechaInicio + ", fechaFin=" + fechaFin + "]";
	}
	

}

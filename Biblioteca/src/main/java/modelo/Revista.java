package modelo;

import utiles.Temas;

public class Revista extends Prestable {
	private String nombreColeccion;
	private int paginas;
	private Temas tema;

	public Revista(String nombreColeccion, int paginas, Temas tema, String tejuelo, String titulo) {
		super(tejuelo, titulo);
		this.nombreColeccion = nombreColeccion;
		this.paginas = paginas;
		this.tema = tema;

	}

	public String getNombreColeccion() {
		return nombreColeccion;
	}

	public int getPaginas() {
		return paginas;
	}

	public Temas getTema() {
		return tema;
	}

	@Override
	public String toString() {
		return "Revista [nombreColeccion=" + nombreColeccion + ", paginas=" + paginas + ", tema=" + tema + "]";
	}

	

}

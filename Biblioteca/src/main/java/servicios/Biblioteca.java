package servicios;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Scanner;

import modelo.Copia;
import modelo.Lector;
import modelo.Libro;
import modelo.Multa;
import modelo.Pelicula;
import modelo.Prestable;
import modelo.Prestamo;
import modelo.Revista;
import utiles.Generos;
import utiles.Temas;

/**
 * Hello world!
 *
 */
public class Biblioteca {
	Scanner sc = new Scanner(System.in);
	private ArrayList<Prestable> prestables;
	private ArrayList<Lector> lectores;

	int nSocio;

	public Biblioteca() {
		this.prestables = new ArrayList<Prestable>();
		this.lectores = new ArrayList<Lector>();
	}

	public void darAltaPrestables() {
		System.out.println("Digame el titulo");
		String titulo = sc.nextLine();

		System.out.println("Digame el tejuelo");
		String tejuelo = sc.nextLine();

		System.out.println("¿Qué tipo de prestable quieres dar de alta?\n1.Libro\n2.Revista\n3.Película");
		int opcion = sc.nextInt();

		Generos generos = null;
		Temas temas = null;
		switch (opcion) {
		case 1:

			System.out.println("Digame autor");
			sc.nextLine();
			String autor = sc.nextLine();

			System.out.println("Digame género\n1.Historia\n2.Informatica\n3.Idiomas");
			String gen = sc.nextLine();

			Libro libro = new Libro(autor, generos.valueOf(gen), titulo, tejuelo);
			prestables.add(libro);
			break;
		case 2:
			System.out.println("Nombre de la coleccion");
			String name = sc.nextLine();
			System.out.println("Nº de páginas");
			int n = sc.nextInt();
			System.out.println("Tema\nNovela\nPoesia\nEnsayo\nTeatro\nComic");
			String tem = sc.nextLine();

			Revista revista = new Revista(name, n, temas.valueOf(tem), titulo, tejuelo);
			prestables.add(revista);
			break;
		case 3:
			System.out.println("Dime minutos");
			String mins = sc.nextLine();
			System.out.println("Dime director");
			String dir = sc.nextLine();
			System.out.println("Dime actor principal");
			String act = sc.nextLine();

			Pelicula pelicula = new Pelicula(mins, dir, act, titulo, tejuelo);
			prestables.add(pelicula);
			break;
		default:
			System.out.println("No ha elegido ninguna opción válida");
			break;

		}

	}

	public void darAltaCopia() {

		System.out.println("Digame el titulo");
		String titulo = sc.nextLine();

		System.out.println("Digame el tejuelo");
		String tejuelo = sc.nextLine();

		System.out.println("¿Qué tipo de copia quieres dar de alta?\n1.Libro\n2.Revista\n3.Película");
		int opcion = sc.nextInt();

		Copia copia;
		int indice = 0;

		switch (opcion) {
		case 1:

			// Creo un libro de prueba para buscarlo en el array, y a ese libro le añado la
			// copia
			Libro libro = new Libro(null, null, titulo, tejuelo);
			indice = prestables.indexOf(libro);

			Libro lib = (Libro) prestables.get(indice);

			// Creo la copia y le pongo un indice para saber cual es
			copia = new Copia(lib.nCopias() + 1);
			copia.setPrestable(lib);
			lib.setCopias(copia);

			break;
		case 2:
			Revista revista = new Revista(null, 0, null, titulo, tejuelo);
			indice = prestables.indexOf(revista);

			Revista revis = (Revista) prestables.get(indice);

			copia = new Copia(revis.nCopias() + 1);
			copia.setPrestable(revis);
			revis.setCopias(copia);

			break;
		case 3:
			Pelicula pelicula = new Pelicula(null, null, null, titulo, tejuelo);
			indice = prestables.indexOf(pelicula);

			Pelicula peli = (Pelicula) prestables.get(indice);

			copia = new Copia(peli.nCopias() + 1);
			copia.setPrestable(peli);
			peli.setCopias(copia);

			break;
		default:
			System.out.println("No ha elegido ninguna opción válida");
			break;

		}
	}

	public void darAltaUsuario() {

		System.out.println("Dime nombre");
		sc.nextLine();
		String nombre = sc.nextLine();

		System.out.println("Dime teléfono");
		String telefono = sc.nextLine();
		System.out.println("Dime direccion");
		String direccion = sc.nextLine();

		Lector usuario = new Lector(nSocio, nombre, telefono, direccion);
		nSocio++;
		lectores.add(usuario);

	}

	private Lector buscarUsuario() {
		System.out.println("Nombre del usuario");
		sc.nextLine();
		String nombre = sc.nextLine();

		System.out.println("Teléfono del usuario");
		String telefono = sc.nextLine();

		Lector lec = new Lector(0, nombre, telefono, null);

		if (lectores.contains(lec)) {
			int indice = lectores.indexOf(lec);
			lec = lectores.get(indice);
		}
		return lec;
	}

	public void darPrestableAUsuario() {
		Lector lec = buscarUsuario();

		if (lec.nPrestables() > 3
				|| lec.getMulta() != null && lec.getMulta().getFechaFin().isAfter(LocalDateTime.now())) {
			// lec.getMulta().getFechaFin().isAfter(LocalDateTime.now())
			System.out.println("No puedes coger un prestable");
		} else {

			System.out.println(
					"¿Cómo quieres buscar el prestable?\n1.Por tipo\n2.Por Titulo\n3.Por Tejuelo\n4.Por Genero");
			int opcion = sc.nextInt();

			int indic;

			switch (opcion) {
			case 1:
				System.out.println("Dime tipo\n1.Libro\n2.Pelicula\n3.Revista");
				int tipo = sc.nextInt();

				switch (tipo) {
				case 1:
					indic = 1;
					for (Prestable p : prestables) {
						if (p instanceof Libro) {
							System.out.println(indic + "" + p.toString());
						}
						indic++;
					}

					añadirPrestamo(lec);
					break;
				case 2:
					indic = 1;
					for (Prestable p : prestables) {
						if (p instanceof Pelicula) {
							System.out.println(indic + "" + p.toString());
						}
						indic++;
					}
					añadirPrestamo(lec);
					break;
				case 3:
					indic = 1;
					for (Prestable p : prestables) {
						if (p instanceof Revista) {
							System.out.println(indic + "" + p.toString());
						}
						indic++;
					}
					añadirPrestamo(lec);
					break;
				}
				break;
			case 2:
				System.out.println("Dime título");
				String titulo = sc.nextLine();
				sc.nextLine();

				indic = 1;

				for (Prestable p : prestables) {
					if (p.getTitulo().equals(titulo)) {
						System.out.println(indic + "" + p.toString());
					}
					indic++;
				}
				añadirPrestamo(lec);
				break;
			case 3:
				System.out.println("Dime Tejuelo");
				String tejuelo = sc.nextLine();
				indic = 1;

				for (Prestable p : prestables) {
					if (p.getTejuelo().equals(tejuelo)) {
						System.out.println(indic + "" + p.toString());
					}
					indic++;
				}
				añadirPrestamo(lec);

				break;
			case 4:
				System.out.println("Dime Género");
				String genero = sc.nextLine();
				indic = 1;

				for (Prestable p : prestables) {
					if (p instanceof Libro) {
						Libro libro = (Libro) p;
						if (libro.getGenero().equals(genero)) {
							System.out.println(indic + "" + p.toString());
						}
					}
					indic++;
				}
				añadirPrestamo(lec);
				break;
			default:
				System.out.println("No ha elegido una opcion correcta");
				break;

			}
		}

	}

	private void añadirPrestamo(Lector lector) {
		System.out.println("¿Cuál quieres?");
		int eleg = sc.nextInt();
		eleg--;

		LocalDateTime today = LocalDateTime.now();
		Prestamo prestamo = new Prestamo(today);

		Copia c = prestables.get(eleg).darCopia();
		if (c == null) {
			System.out.println("No hay copias disponible de este prestable");
		} else {

			prestamo.setCopia(c);

			c.setEstadoCopia(true);

			lector.añadirPrestamos(prestamo);
		}
	}

	public void devolverPrestable() {
		Lector lec = buscarUsuario();

		try {
			for (int i = 0; i < lec.nPrestables(); i++) {
				System.out.println((1 + i) + "" + lec.darPrestamo(i).getCopia().toString());
			}
			System.out.println("Que copia quieres devolver");
			int p = sc.nextInt();
			p--;
			// marcar la copia como no prestada
			lec.darPrestamo(p).getCopia().setEstadoCopia(false);

			LocalDateTime fecha_fin = lec.darPrestamo(p).getFechaInicio().plusSeconds(10);

			lec.darPrestamo(p).setFechFin(fecha_fin);

			lec.setPrestamosHistorico(lec.darPrestamo(p));

			lec.darPrestamo(p).resetPrestamo();

			if (LocalDateTime.now().isAfter(fecha_fin)) {
				System.out.println("Se ha pasado de la fecha limite, va a ser multado");
				long finMulta = Duration.between(fecha_fin, LocalDateTime.now()).getSeconds();

				Multa multa = new Multa(fecha_fin, fecha_fin.plusSeconds(finMulta * 2));

				lec.añadirMulta(multa);
				lec.setMultasHistorico(multa);
			}
		} catch (Exception e) {
			System.out.println("ha ocurrido un error, revise que todos los datos"
					+ "\n estan debidamente intoducidos");
		}

	}

	public void imprimirPrestables() {
		System.out.println("¿Qué prestables quiere imprimir?\n1.Libros\n2.Revistas\n3.Películas\n4.Todos");
		int opcion = sc.nextInt();
		switch (opcion) {
		case 1:
			for (Prestable p : prestables) {
				if (p instanceof Libro) {
					System.out.println(p.toString());
				}
			}
			break;
		case 2:
			for (Prestable p : prestables) {
				if (p instanceof Libro) {
					System.out.println(p.toString());
				}
			}
			break;
		case 3:
			for (Prestable p : prestables) {
				if (p instanceof Libro) {
					System.out.println(p.toString());
				}
			}
			break;
		case 4:
			for (Prestable p : prestables) {
				System.out.println(p.toString());
			}
			break;

		}
	}

	public void imprimirUsuarios() {
		System.out.println("Aquí tiene un listado de usuarios");
		for (Lector l : lectores) {
			System.out.println(l.toString());
		}
	}

	public void imprimirUsuariosMultados() {
		System.out.println("Aquí tiene un listado de usuarios multados");
		for (Lector l : lectores) {
			if (l.getMulta().getFechaFin().isAfter(LocalDateTime.now())) {
				System.out.println(l.toString());
			}
		}
	}

	public void imprimirUsuario() {
		Lector lec = buscarUsuario();
		lec.imprimirHistoricos();
	}

	public void darDeBajaUsuario() {
		Lector lec = buscarUsuario();

		lec.devolverCopias();

		lec = null;
	}
}

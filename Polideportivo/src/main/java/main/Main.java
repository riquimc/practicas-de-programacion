/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import Sercivios.Polideportivo;
import java.util.Scanner;

/**
 *
 * @author CAR
 */
public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        Polideportivo polideportivo = new Polideportivo();
            boolean salir = false; 
            
        System.out.println("Bienvenido/a al programa de gestion del polideportivo CC \n\n");
        do {
            System.out.println("¿Qué acción desea ralizar?\n 1.Dar de alta a un alumno\n"
                    + "2.Matricular a un alumno en una actividad\n"
                    + "3.Dar de baja a un alumno en el sistema\n"
                    + "4.Generar recibo de pago de alumno\n"
                    + "5.Generar dinero recaudado por cada activida \n"
                    + "6.Salir\n"
                    + "ATENCION: debe haber dado de alta a un mínimo de 1 alumno en el polideportivo antes de realizar las siguientes acciones)");

            int opcion = sc.nextInt();
            switch (opcion) {
                case 1:
                    polideportivo.darAltaAlumno();
                    break;
                case 2:
                    polideportivo.matricularAlumno();
                    break;
                case 3:
                    polideportivo.darBajaAlumno();
                    break;
                case 4:
                    polideportivo.generarRecibo();
                    break;
                case 5:
                    polideportivo.generarReciboActividad();
                    break;
                case 6:
                    salir=true;
                    break;
                default:
                    System.out.println("No ha introducido un número correcto");
                    break;

            }

        } while (!salir);
    }

}

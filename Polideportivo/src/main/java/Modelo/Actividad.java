/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import Utilities.DiasSemana;
import java.util.ArrayList;

/**
 *
 * @author CAR
 */
public class Actividad {

    private String tipo;
    private DiasSemana dias;
    private String horario;
    private int plazas;
    private double coste;

    private ArrayList<Alumno> alumnos;

    public Actividad(String tipo, DiasSemana dias, String horario, double coste, int plazas) {
        this.tipo = tipo;
        this.dias = dias;
        this.horario = horario;
        this.coste = coste;
        this.plazas = plazas;
        alumnos = new ArrayList<>();
    }

    public String getTipo() {
        return tipo;
    }

    public DiasSemana getDias() {
        return dias;
    }

    public String getHorario() {
        return horario;
    }

    public double getPlazas() {
        return plazas;
    }

    public double getCoste() {
        return coste;
    }

    public int getPlazasEmpleadas() {
        return this.alumnos.size();
    }

    public void setAlumnos(Alumno a) {
        alumnos.add(a);
    }

    public void borrarAlumno(Alumno a) {
        boolean estar = alumnos.contains(a);
        if (estar) {
            alumnos.remove(a);
        }
    }

    @Override
    public String toString() {
        String mensaje = "Actividad{" + "tipo=" + tipo + ", horario=" + horario + "}";
        return mensaje;
    }

}

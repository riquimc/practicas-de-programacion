/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sercivios;

import Modelo.Actividad;
import Modelo.Alumno;
import static Utilities.Constantes.ACTIVIDAD1;
import static Utilities.Constantes.ACTIVIDAD2;
import static Utilities.Constantes.ACTIVIDAD3;
import Utilities.DiasSemana;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author CAR
 */
public class Polideportivo {

    Scanner sc = new Scanner(System.in);
    private ArrayList<Actividad> actividades;
    private ArrayList<Alumno> alumnos;

    public Polideportivo() {
        actividades = new ArrayList<>();
        alumnos = new ArrayList<>();

        crearActividades();

    }
    //Mañana no hay primera hoy tampoco

    private void crearActividades() {
        
        DiasSemana d1 = DiasSemana.LXV;
        DiasSemana d2 = DiasSemana.MJ;

        String ae = ACTIVIDAD1;
        
        actividades.add(new Actividad(ae, d1, "10:10-11:00", 47.60, 35));
        actividades.add(new Actividad(ae, d1, "11:10-12:00", 47.60, 35));
        actividades.add(new Actividad(ae, d1, "19:10-20:00", 47.60, 35));
        actividades.add(new Actividad(ae, d2, "09:10-10:00", 35.70, 35));
        actividades.add(new Actividad(ae, d2, "10:10-11:00", 35.70, 35));
        actividades.add(new Actividad(ae, d2, "18:10-19:00", 35.70, 35));
        actividades.add(new Actividad(ACTIVIDAD2, d1, "19:10-20:00", 45.00, 15));
        actividades.add(new Actividad(ACTIVIDAD2, d2, "18:00-19:00", 34.00, 15));
        actividades.add(new Actividad(ACTIVIDAD2, d2, "19:00-20:00", 34.00, 15));
        actividades.add(new Actividad(ACTIVIDAD3, d1, "18:10-19:00", 47.60, 35));
        actividades.add(new Actividad(ACTIVIDAD3, d2, "17:10-18:00", 35.70, 35));

    }

    public void darAltaAlumno() {
        System.out.println("Ponga el nombre y apellido");
        System.out.print("Nombre: ");
        String nombre = sc.nextLine();
        System.out.println("");
        System.out.print("Apellido: ");
        String apellido = sc.nextLine();
        Alumno prueba = new Alumno(nombre, apellido);
        if (alumnos.contains(prueba)) {
            System.out.println("Este alumno ya existe");
        } else {
            alumnos.add(new Alumno(nombre, apellido));
        }
    }

    public void matricularAlumno() {

        System.out.println("¿Que alumno quiere matricular?");
        System.out.print("Nombre: ");
        String nombre = sc.nextLine();
        System.out.println("");
        System.out.print("Apellido: ");
        String apellido = sc.nextLine();
        boolean existir = alumnos.contains(new Alumno(nombre, apellido));
        boolean cero = true;
        if (existir) {
            System.out.println("¿Como quieres buscar?\n1.Por Actividades\n2.Por Horarios ");
            int eleg = sc.nextInt();
            if (eleg == 1) {
                System.out.println("¿A que actividad quieres inscribirle?(introduzca un número por favor)\n1.Natación\n2.Artes Marciales\n3.Aerobic");
                int choose = sc.nextInt();
                String type;

                switch (choose) {
                    case 1:
                        type = "Natación";
                        cero = mostrarActividades(type);

                        break;
                    case 2:
                        type = "Artes Marciales";
                        cero = mostrarActividades(type);
                        break;
                    case 3:
                        type = "Aerobic";
                        cero = mostrarActividades(type);
                        break;
                    default:
                        break;

                }
                if (cero) {
                    System.out.println("¿Que horario quieres?");
                    int ls = sc.nextInt();
                    ls--;
                    registrarAlumno(nombre, apellido, ls);
                } else {
                    System.out.println("No hay actividades libres");
                }
            } else {
                for (int i = 0; i < actividades.size(); i++) {
                    if (actividades.get(i).getPlazasEmpleadas() < actividades.get(i).getPlazas()) {
                        System.out.print((i + 1) + ".");
                        System.out.println(actividades.get(i).toString());
                        cero = false;
                    }
                }
                if (cero) {
                    System.out.println("¿Que horario quieres?");
                    int ls = sc.nextInt();
                    ls--;
                    registrarAlumno(nombre, apellido, ls);
                } else {
                    System.out.println("No hay actividades libres");
                }

            }

        } else {
            System.out.println("Este alumno no esta en el sistema");
        }
        // buscar alumno

        // buscar actividad
        // buscar horario
        //add alumno a actividad.
    }

    private boolean mostrarActividades(String type) {
        boolean cero = true;
        for (int i = 0; i < actividades.size(); i++) {
            if (actividades.get(i).getPlazasEmpleadas() < actividades.get(i).getPlazas()
                    && actividades.get(i).getTipo().equals(type)) {
                System.out.print((i + 1) + ".");
                System.out.println(actividades.get(i).toString());
                cero = false;
            }
        }
        return cero;
    }

    private void registrarAlumno(String nombre, String apellido, int ls) {

        Alumno alumno = new Alumno(nombre, apellido);

        int indice = alumnos.indexOf(alumno);
        //añado al alumno en la lista del grupo
        actividades.get(ls).setAlumnos(alumnos.get(indice));
        //añado a la actividad en la lista(por lista se entiende array) de actividades del alumno
        alumnos.get(indice).addActividad(actividades.get(ls));

    }

    public void generarRecibo() {
        double recibo = 0;

        System.out.println("¿Que alumno quieres?");
        System.out.print("Nombre: ");
        sc.nextLine();
        String nombre = sc.nextLine();
        System.out.println("");
        System.out.print("Apellido: ");
        String apellido = sc.nextLine();

        Alumno alumno = new Alumno(nombre, apellido);

        boolean estar = alumnos.contains(alumno);

        if (estar) {
            int indice = alumnos.indexOf(alumno);
            recibo = alumnos.get(indice).getDineroAPagar();
            System.out.println("El recibo de " + alumnos.get(indice).toString() + " es de: " + recibo + "€");
        } else {
            System.out.println("El alumno no esta en el sistema");
        }
        // buscar alumno.

        //recorrer sus actividades
    }

    public void darBajaAlumno() {
        // busca Alumno
        System.out.println("¿Que alumno quieres dar de baja?");
        System.out.print("Nombre: ");
        String nombre = sc.nextLine();
        sc.nextLine();
        System.out.println("");
        System.out.print("Apellido: ");
        String apellido = sc.nextLine();

        Alumno alumno = new Alumno(nombre, apellido);
        boolean estar = alumnos.contains(alumno);

        if (estar) {
            int indice = alumnos.indexOf(alumno);
            alumnos.get(indice).deleteActividades();
            alumnos.remove(alumno);
            for (int j = 0; j < actividades.size(); j++) {
                actividades.get(j).borrarAlumno(alumno);
            }
        } else {
            System.out.println("El alumno introducido no se encuentra en el sistema");
        }

        // darle de baja en array de alumnos
        // darle de baja en todas las actividades del alumno.
    }

    public void generarReciboActividad() {
        // buscar actividad
        double recibo;
        for (Actividad a : actividades) {
            recibo = (a.getCoste() * a.getPlazasEmpleadas());
            System.out.println(a.toString() + " .Dinero recaudado: " + recibo + "€ (vamos lo que se va a gastar el profesor en skins para el lol)");
        }

        // multiplicar precio por plazas.
    }
}

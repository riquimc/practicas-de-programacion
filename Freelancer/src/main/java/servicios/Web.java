package servicios;

import java.util.ArrayList;
import java.util.Scanner;

import modelo.Analista;
import modelo.Experiencia;
import modelo.Freelance;
import modelo.Proyecto;
import modelo.ProyectoDesarrollo;
import modelo.Puesto;

public class Web {

	Scanner sc = new Scanner(System.in);
	private ArrayList<Freelance> freelancers;
	private ArrayList<Proyecto> proyectos;

	public Web() {
		this.freelancers = new ArrayList<Freelance>();
		this.proyectos = new ArrayList<Proyecto>();
	}

	public void darAltaFreelance() {
		System.out.println("Dime nombre");
		String nombre = sc.nextLine();
		System.out.println("Dime dni");
		String dni = sc.nextLine();
		System.out.println("Dime salario hora mínimo");
		int salarioMin = sc.nextInt();

		ArrayList<Experiencia> llevar = new ArrayList<Experiencia>();
		System.out.println("¿Cuantos lenguajes de programación sabes?");
		int numLen = sc.nextInt();

		for (int i = 0; i < numLen; i++) {
			System.out.println("Dime nombre");
			String lenguaje = sc.nextLine();
			System.out.println("Dime cuanta experiencia tienes(siendo 1 muy poco y 10 mucha experiencia)");
			int expDes = sc.nextInt();
			Experiencia experiencia = new Experiencia(lenguaje, expDes);
			llevar.add(experiencia);
		}
		System.out.println("¿Es un analista o un desarrollador?\n1.Analista\n2.Desarrollador");
		int opcion = sc.nextInt();
		sc.nextLine();
		if (opcion == 1) {
			System.out.println("Dime experiencia que tengas con proyectos de analisis(1-10)");
			int expAnalist = sc.nextInt();
			System.out.println("Dime cuanto cobras por hora");
			double cobrar = sc.nextDouble();

			Analista analista = new Analista(dni, nombre, salarioMin, expAnalist, cobrar);
			añadirExperiencia(numLen, llevar, analista);
			freelancers.add(analista);
		} else {
			Analista analista = new Analista(dni, nombre, salarioMin, 0, 0);
			añadirExperiencia(numLen, llevar, analista);
			freelancers.add(analista);
		}
	}

	private void añadirExperiencia(int numLen, ArrayList<Experiencia> llevar, Analista analista) {
		for (int i = 0; i < numLen; i++) {
			analista.addExperiencia(llevar.get(i));
		}
	}

	public void darAltaProyecto() {
		System.out.println("Dime nombre");
		String nombre = sc.nextLine();
		System.out.println("Dime lugar");
		String lugar = sc.nextLine();

		System.out.println("¿Qué tipo de proyecto es?\n1.De Desarrollo\n2.De Analisis");
		int opcion = sc.nextInt();
		ProyectoDesarrollo proyecto;
		if (opcion == 2) {
			proyecto = new ProyectoDesarrollo(nombre, lugar, null);
		} else {
			System.out.println("nombre del lenguaje en el que se dearrolla");
			String nombreLeng = sc.nextLine();
			proyecto = new ProyectoDesarrollo(nombre, lugar, nombreLeng);
		}
		System.out.println("Cuantos puestos tiene el proyecto");
		int pues = sc.nextInt();

		for (int i = 0; i < pues; i++) {
			System.out.println("Dime nombre");
			String nombrePuesto = sc.nextLine();
			System.out.println("Dime salario minimo del puesto");
			double salMin = sc.nextDouble();
			System.out.println("Dime experiencia minima del puesto");
			int expMinima = sc.nextInt();

			Puesto puesto = new Puesto(expMinima, nombrePuesto, salMin);
			proyecto.AddPuesto(puesto);
			proyectos.add(proyecto);
		}
	}

	public void añadirFreelanceProyecto() {
		int indice = buscarProyecto();
		if (indice == -1) {
			System.out.println("Este proyecto no existe");
		} else {
			ProyectoDesarrollo proyecto = (ProyectoDesarrollo) proyectos.get(indice);
			
			System.out.println("Busca el freelance");
			System.out.println("Dime nombre");
			String nombre = sc.nextLine();
			System.out.println("Dime dni");
			String dni = sc.nextLine();
			
			Analista a= new Analista(dni,nombre,0,0,0);
			int index=freelancers.indexOf(a);
			if(index==-1) {
				System.out.println("No existe este freelance");
			}else {
				Analista analista=(Analista)freelancers.get(index);
				proyecto.añadirFreelancePuesto(analista);
			}
		}
	}
	
	public void listadoCostePoyectos() {
		for(Proyecto p:proyectos) {
			ProyectoDesarrollo o= (ProyectoDesarrollo) p;
			System.out.println(p.toString()+" Este proyecto ha costado "+o.costeProyectos()+"€");
			
		}
	}

	private int buscarProyecto() {
		System.out.println("Busca el proyecto");
		System.out.println("Dime nombre");
		String nombre = sc.nextLine();
		System.out.println("Dime lugar");
		String lugar = sc.nextLine();
		ProyectoDesarrollo o = new ProyectoDesarrollo(nombre, lugar, null);

		return proyectos.indexOf(o);

	}
}

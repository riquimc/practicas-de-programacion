package modelo;

import java.util.ArrayList;

public abstract class Proyecto {

	private String nombre;
	private String lugar;
	protected ArrayList<Puesto> puestos;

	public Proyecto(String nombre, String lugar) {
		this.nombre = nombre;
		this.lugar = lugar;
		this.puestos = new ArrayList<Puesto>();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((lugar == null) ? 0 : lugar.hashCode());
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		return result;
	}
	
	

	@Override
	public String toString() {
		return "Proyecto [nombre=" + nombre + ", lugar=" + lugar + "]";
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Proyecto other = (Proyecto) obj;
		if (lugar == null) {
			if (other.lugar != null)
				return false;
		} else if (!lugar.equals(other.lugar))
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		return true;
	}

	public void añadirFreelancePuesto(Freelance f) {
		int i = 0;
		Analista a= (Analista) f;
		boolean salir = false;
		while (!salir) {
			if (puestos.get(i).comprobrarFreelance()==-1
					&& a.getNivelExperiencia()>=puestos.get(i).getNivelExpMin()
					&& a.getPrecioHora()>=puestos.get(i).getSalarioHora()) {
				this.puestos.get(i).setFreelance(a);
				salir = true;
			} else {
				i++;
				if(i>puestos.size()) {
					salir = true;
				}
			}
		}
	}

	public void AddPuesto(Puesto a) {
		puestos.add(a);
	}


	public String getNombre() {
		return nombre;
	}

	public String getLugar() {
		return lugar;
	}

}

package modelo;

public class Experiencia {

	private String nombreLenguaje;
	private int experiencia;

	public Experiencia(String nombreLenguaje, int experiencia) {
		this.nombreLenguaje = nombreLenguaje;
		this.experiencia = experiencia;
	}

	public String getNombreLenguaje() {
		return nombreLenguaje;
	}

	public int getExperiencia() {
		return experiencia;
	}
	
	
}

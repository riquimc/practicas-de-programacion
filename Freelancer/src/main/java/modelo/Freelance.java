package modelo;

import java.util.ArrayList;
import java.util.Scanner;

public abstract class Freelance {

	Scanner sc = new Scanner(System.in);
	private String dni;
	private String nombre;
	private double salarioMin;
	private ArrayList<Experiencia> experiencia;

	public Freelance(String dni, String nombre, double salarioMin) {
		this.dni = dni;
		this.nombre = nombre;
		this.salarioMin = salarioMin;
		experiencia = new ArrayList<Experiencia>();
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dni == null) ? 0 : dni.hashCode());
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Freelance other = (Freelance) obj;
		if (dni == null) {
			if (other.dni != null)
				return false;
		} else if (!dni.equals(other.dni))
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		return true;
	}

	public String getDni() {
		return dni;
	}

	public String getNombre() {
		return nombre;
	}

	public int devolverExpLenguaje() {
		System.out.println("Dime nombre de lenguaje");
		String nombre=sc.nextLine();
		boolean salir=false;
		int exp=0;
		for(int i=0;i<experiencia.size() && !salir;i++) {
			if(nombre.equals(experiencia.get(i).getNombreLenguaje())) {
				 exp=experiencia.get(i).getExperiencia();
				 salir=true;
			}
		}
		return exp;
	}

	public double getSalarioMin() {
		return salarioMin;
	}

	public void addExperiencia(Experiencia a) {
		experiencia.add(a);
	}

	public Experiencia encontrarExperiencia(int i) {
		return experiencia.get(i);
	}

}

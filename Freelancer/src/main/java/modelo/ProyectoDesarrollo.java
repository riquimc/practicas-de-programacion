package modelo;

import java.util.ArrayList;

public class ProyectoDesarrollo extends Proyecto {

	private String lenguaje;

	public ProyectoDesarrollo(String nombre, String lugar, String lenguaje) {
		super(nombre, lugar);
		this.lenguaje = lenguaje;
	}

	public void añadirFreelancePuesto(Freelance f) {
		int i = 0;
		Analista a = (Analista) f;
		boolean salir = false;
		while (!salir) {
			if (puestos.get(i).comprobrarFreelance()==-1
					&& a.devolverExpLenguaje() >= puestos.get(i).getNivelExpMin()
					&& a.getSalarioMin() >= puestos.get(i).getSalarioHora()) {
				this.puestos.get(i).setFreelance(a);
				salir = true;
			} else {
				i++;
				if (i > puestos.size()) {
					salir = true;
				}
			}
		}
	}

	public double costeProyectos() {
		double resultado = 0;
		for (Puesto p : puestos) {

			if (p.getFreelance() instanceof Analista) {
				Analista a = (Analista) p.getFreelance();
				resultado = a.getPrecioHora() + resultado;
			} else {
				resultado = p.getFreelance().getSalarioMin() + resultado;
			}
		}
		return resultado;
	}

	public String getLenguaje() {
		return lenguaje;
	}

}

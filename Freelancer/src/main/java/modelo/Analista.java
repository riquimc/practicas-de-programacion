package modelo;

public class Analista extends Freelance {
	
	private int nivelExperiencia;
	private double precioHora;
	
	public Analista(String dni, String nombre,double salarioMin, int nivelExperiencia, double precioHora) {
		super(dni, nombre, salarioMin);
		this.nivelExperiencia = nivelExperiencia;
		this.precioHora = precioHora;
	}

	public int getNivelExperiencia() {
		return nivelExperiencia;
	}

	public double getPrecioHora() {
		return precioHora;
	}
	
	

}

package modelo;

public class Puesto {

	private int nivelExpMin;
	private String nombre;
	private double salarioHora;
	private Freelance freelance;

	public Puesto(int nivelExpMin, String nombre, double salarioHora) {
		this.nivelExpMin = nivelExpMin;
		this.nombre = nombre;
		this.salarioHora = salarioHora;
	}

	public int getNivelExpMin() {
		return nivelExpMin;
	}

	public String getNombre() {
		return nombre;
	}

	public int comprobrarFreelance() {
		int comprobar = 0;
		if (freelance != null) {
			comprobar = 1;
		} else {
			comprobar = -1;
		}
		return comprobar;
	}

	public void setFreelance(Freelance freelance) {
		this.freelance = freelance;
	}

	public double getSalarioHora() {
		return salarioHora;
	}

	public Freelance getFreelance() {
		return freelance;
	}

}

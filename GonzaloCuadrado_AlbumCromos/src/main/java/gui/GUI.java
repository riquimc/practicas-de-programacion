/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import java.util.ArrayList;
import java.util.Scanner;
import modelo.Album;
import modelo.Cromo;
import modelo.Niñato;
import servicios.Parque_jeringuillas;

/**
 *
 * @author CAR
 */
public class GUI {

	 private Parque_jeringuillas parque = new Parque_jeringuillas();

	    public void darAltaNiñato(Scanner sc) {

	        System.out.println("¿Cómo se llama el niñato?");
	        String niñato = sc.nextLine();
	        Niñato a = new Niñato(niñato);

	        boolean existir = parque.darAltaNiñato(a);
	        if (!existir) {
	            System.out.println("Ya hay un chaval con este nombre");
	        }
	    }

	    public void darAltaAlbum(Scanner sc) {

	        System.out.println("¿Cómo se llama el álbum?");
	        String nombre_album = sc.nextLine();
	        int num_paginas;
	        do {
	            System.out.println("¿Cuántos cromos tiene?");
	            try {
	                num_paginas = sc.nextInt();
	                sc.nextLine();
	                if (num_paginas <= 0) {
	                    System.out.println("Tiene que ser un número positivo");
	                }
	            } catch (Exception e) {
	                System.out.println("Introduce un número");
	                num_paginas = -1;
	            }
	        } while (num_paginas <= 0);

	        Album album = new Album(nombre_album, num_paginas);
	        mostrarNiños();
	        int opcion;
	        do {
	            System.out.println("¿A qué niño se lo quieres comprar?");
	            try {
	                opcion = sc.nextInt();
	                
	            } catch (Exception e) {
	                 sc.nextLine();
	                System.out.println("No existe este niño");
	                opcion = -1;
	            }
	        } while (opcion < 0 || opcion > parque.getNiñatos().size() - 1);

	        parque.darAltaAlbum(nombre_album, num_paginas, opcion);

	    }

	    public void abrirSobre(Scanner sc) {
	        Niñato a = pedirNiño(sc);
	        Album album_elegido = pedirAlbum(a, sc);
	       

	        parque.abrirSobre(a, album_elegido);
	        album_elegido.imprimirCromos();
	        if (album_elegido.isCompletado()) {
	            System.out.println("Has terminado el album de: " + album_elegido.getNombre() + " en: "
	                    + album_elegido.getTiempo_completado() + " segundos");
	        }
	    }

	    public void cambiarCromos(Scanner sc) {
	        Niñato niño_busca_cromo = pedirNiño(sc);
	        Album album_niño_busca_cromo = pedirAlbum(niño_busca_cromo, sc);
	        album_niño_busca_cromo.imprimirCromos();
	        if (!album_niño_busca_cromo.isVacio()) {
	            int id_cromo;
	            int id_cromo_ofrecido;
	            do {
	                System.out.println("¿Qué cromo necesitas");
	                try {
	                    id_cromo = sc.nextInt();
	                    sc.nextLine();
	                } catch (Exception e) {

	                    System.out.println("Introduce un número");
	                    id_cromo = -1;
	                }
	            } while (id_cromo < 0);
	            do {
	                System.out.println("¿Qué cromo ofreces?");
	                try {
	                    id_cromo_ofrecido = sc.nextInt();
	                } catch (Exception e) {
	                    sc.nextLine();
	                    System.out.println("Introduce un número");
	                    id_cromo_ofrecido = -1;
	                }
	            } while (id_cromo_ofrecido < 0);

	            Cromo cromo_ofrecido = new Cromo(id_cromo_ofrecido);
	            Cromo cromo_buscado = new Cromo(id_cromo);
	            // Se comprueba si el niño tiene el ejemplar del cromo que ofrece
	            if (album_niño_busca_cromo.hayCromoOfrecido(cromo_ofrecido)) {
	                // Se busca si hay otro niño que tenga ese album, tenga repetido el cromo que se
	                // busca y le falte el que se ofrece
	                Niñato niño_con_cromo = parque.verSiOtrosNiñosTienenCromo(cromo_buscado, album_niño_busca_cromo,
	                        cromo_ofrecido);

	                if (niño_con_cromo != null) {
	                    // Si el niño existe, se intercambian los cromos.
	                    Album album_niño_con_cromo = niño_con_cromo.buscarAlbum(album_niño_busca_cromo);
	                    System.out.println("Intercambiados!");
	                    System.out.println(niño_busca_cromo.getNombre() + ": ");
	                    album_niño_busca_cromo.imprimirCromos();

	                    System.out.println(niño_con_cromo.getNombre() + ": ");
	                    album_niño_con_cromo.imprimirCromos();

	                    // Se comprueba si al realizar el intercambio, uno o ambos niños completan sus
	                    // álbumes
	                    if (album_niño_busca_cromo.isCompletado()) {

	                        System.out.println(niño_busca_cromo.getNombre() + " ha completado el album: "
	                                + album_niño_busca_cromo.getNombre() + " en: "
	                                + album_niño_busca_cromo.getTiempo_completado() + " segundos");

	                    }
	                    if (album_niño_con_cromo.isCompletado()) {
	                        System.out.println(niño_con_cromo.getNombre() + " ha completado el album: "
	                                + album_niño_con_cromo.getNombre() + " en: "
	                                + album_niño_con_cromo.getTiempo_completado() + " segundos");
	                    }
	                } else {
	                    System.out.println("No hay posibilidades de cambio");
	                }
	            } else {
	                System.out.println("No tienes el cromo que ofreces o solo tienes un ejemplar");
	            }
	        } else {
	            System.out.println("Este niño no tiene cromos de este album, no puede cambiar");
	        }

	    }

	    public void verAlbumesCompletados(Scanner sc) {
	        Niñato niño = pedirNiño(sc);
	        ArrayList<Album> albumes_completados = niño.listaalbumesCompletados();
	        if (albumes_completados.isEmpty()) {
	            System.out.println("Este niño no ha completado ningún álbum");
	        } else {
	            for (Album e : albumes_completados) {
	                System.out.println(albumes_completados.indexOf(e) + "." + e.getNombre() + " completado en: "
	                        + e.getTiempo_completado() + " segundos");
	            }
	        }
	    }

	    public void mostrarNiños() {
	        for (Niñato e : parque.getNiñatos()) {
	            System.out.println(parque.getNiñatos().indexOf(e) + "." + e.getNombre());
	        }
	    }

	    public Niñato pedirNiño(Scanner sc) {
	        mostrarNiños();
	        int opcion_niño;
	        do {
	            System.out.println("Seleccion un niño: ");
	            try {
	                opcion_niño = sc.nextInt();
	                if ((opcion_niño < 0 || opcion_niño > parque.getNiñatos().size() - 1)) {
	                    System.out.println("Este niño no existe, prueba otro");
	                }
	            } catch (Exception e) {
	                sc.nextLine();
	                System.out.println("Este niño no existe, prueba otro");
	                opcion_niño = -1;
	            }
	        } while (opcion_niño < 0 || opcion_niño > parque.getNiñatos().size() - 1);

	        return parque.pedirNiño(opcion_niño);
	    }

	    private Album pedirAlbum(Niñato niñato, Scanner sc) {
	        imprimirAlbumes(niñato);
	        int opcion_album;
	        do {
	            System.out.println("¿Para qué album es?");
	            try {
	                opcion_album = sc.nextInt();
	                if (opcion_album < 0 || opcion_album > niñato.getNumAlbumes() - 1) {
	                    System.out.println("Introduce un número de la lista");
	                }
	            } catch (Exception e) {
	                sc.nextLine();
	                System.out.println("Introduce un número");
	                opcion_album = -1;
	            }
	        } while (opcion_album < 0 || opcion_album > niñato.getNumAlbumes() - 1);
	        return parque.pedirAlbum(niñato, sc, opcion_album);
	    }
	    
	     public void imprimirAlbumes(Niñato a) {
	        for (Album e : a.getAlbumes()) {
	            System.out.println(a.getAlbumes().indexOf(e) + "." + e.getNombre());
	        }
	    }
	     
	      public void addCromo(boolean estar) {
	        if (!estar) {
	            System.out.println("Ya has completado este álbum, no malgastes el dinero.");
	        }
	    }
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servicios;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;
import modelo.Album;
import modelo.Cromo;
import modelo.Niñato;

/**
 *
 * @author dam1
 */
public class Parque_jeringuillas {

	private ArrayList<Niñato> niñatos;

	public ArrayList<Niñato> getNiñatos() {
		return niñatos;
	}

	public Parque_jeringuillas() {
		niñatos = new ArrayList<Niñato>();
		niñatos.add(new Niñato("Lucas"));
		Niñato a = new Niñato("Lopez");
		niñatos.add(a);
		a.addAlbum("Pokemon", 9);

	}

	public boolean darAltaNiñato(Niñato a) {
		boolean estar = false;
		if (!niñatos.contains(a)) {
			estar = true;
			niñatos.add(a);
		}
		return estar;
	}

	public boolean darAltaAlbum(String nombre_album, int num_paginas, Niñato ñ) {
		boolean estar = false;
		if (!niñatos.get(niñatos.indexOf(ñ)).getAlbumes().contains(new Album(nombre_album, num_paginas))) {
			niñatos.get(niñatos.indexOf(ñ)).addAlbum(nombre_album, num_paginas);
			estar = true;
		}
		return estar;
	}

	public ArrayList<Cromo> abrirSobre(Niñato a, Album album_elegido) {
		Random r = new Random();
		ArrayList<Cromo> cromosDados = new ArrayList<Cromo>();
		int num_cromos = a.getNumCromosAlbum(album_elegido);

		for (int i = 0; i < 2 && !album_elegido.isCompletado(); i++) {
			int id_cromo = r.nextInt(num_cromos);
			Cromo cromo = new Cromo(id_cromo);
			cromosDados.add(cromo);
			album_elegido.addCromo(cromo);
			album_elegido.comprobarSiAcabado();
		}
		return cromosDados;
	}

	public void cambiarCromos(Album album_niño_busca_cromo, Cromo cromo_buscado, Cromo cromo_ofrecido) {
		if (!album_niño_busca_cromo.isVacio()) {
			if (album_niño_busca_cromo.hayCromoOfrecido(cromo_ofrecido)) {
				Niñato niño_con_cromo = verSiOtrosNiñosTienenCromo(cromo_buscado, album_niño_busca_cromo,
						cromo_ofrecido);
				if (niño_con_cromo != null) {
					Album album_niño_con_cromo = niño_con_cromo.buscarAlbum(album_niño_busca_cromo);

					album_niño_busca_cromo.addCromo(cromo_buscado);
					album_niño_busca_cromo.darCromo(cromo_ofrecido);
					album_niño_busca_cromo.comprobarSiAcabado();
					album_niño_con_cromo.addCromo(cromo_ofrecido);
					album_niño_con_cromo.darCromo(cromo_buscado);
					album_niño_con_cromo.comprobarSiAcabado();
				}
			}
		}
	}

	public Niñato verSiOtrosNiñosTienenCromo(Cromo cromo_buscado, Album album_cromo, Cromo cromo_ofrecido) {
		Niñato niño_cromo = null;
		for (Niñato e : niñatos) {
			if (e.comprobarSiTieneCromoBuscado(cromo_buscado, album_cromo, cromo_ofrecido)) {
				niño_cromo = e;
			}
		}
		return niño_cromo;
	}

	public Niñato pedirNiño(int opcion_niño) {
		return niñatos.get(opcion_niño);
	}

	public Album pedirAlbum(Niñato niñato, Scanner sc, int opcion_album) {
		return niñato.getAlbum(opcion_album);
	}

}

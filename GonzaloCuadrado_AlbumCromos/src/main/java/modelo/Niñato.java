/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.util.ArrayList;
import java.util.Objects;

/**
 *
 * @author dam1
 */
public class Niñato {

    private String nombre;
    private ArrayList<Album> albumes;

    public Niñato(String nombre) {
        this.nombre = nombre;
        albumes = new ArrayList<Album>();
    }

    public void addAlbum(String nombre, int num_cromos) {
        albumes.add(new Album(nombre, num_cromos));
    }

    public String getNombre() {
        return nombre;
    }

    public void imprimirAlbumes() {
        for (Album e : albumes) {
            System.out.println(albumes.indexOf(e) + "." + e.getNombre());
        }
    }


    public boolean niñoTieneCromoOfrecido(Cromo cromo, Album album) {
        boolean tiene_cromo = true;
        if (!albumes.get(albumes.indexOf(album)).hayCromoOfrecido(cromo)) {
            tiene_cromo = false;
        }
        return tiene_cromo;
    }

    public int getNumCromosAlbum(Album album) {
        return albumes.get(albumes.indexOf(album)).getNum_cromos();
    }

    public boolean comprobarSiTieneCromoBuscado(Cromo cromo_buscado, Album album_cromo, Cromo cromo_ofrecido) {
        boolean tiene_cromo = false;
        Album a = null;
        try {
            a = albumes.get(albumes.indexOf(album_cromo));
        } catch (Exception e) {
            System.out.println(this.nombre + " no tiene este álbum");
        }
        if (a != null) {
            tiene_cromo = a.hayCromoRepetido(cromo_buscado, cromo_ofrecido);
        }
        return tiene_cromo;
    }

    public Album getAlbum(int opcion_album) {
        return albumes.get(opcion_album);
    }

    public Album buscarAlbum(Album album) {
        return albumes.get(albumes.indexOf(album));
    }

    public int getNumAlbumes() {
        return albumes.size();
    }

    public ArrayList<Album> listaalbumesCompletados() {
        ArrayList<Album> albumes_completados = new ArrayList<Album>();
        for (Album e : albumes) {
            if (e.isCompletado()) {
                albumes_completados.add(e);
            }
        }
        return albumes_completados;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Niñato other = (Niñato) obj;
        if (!Objects.equals(this.nombre, other.nombre)) {
            return false;
        }
        return true;
    }

	public ArrayList<Album> getAlbumes() {
		return albumes;
	}

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 *
 * @author dam1
 */
public class Cromo {

    private int num_cromo;
    private int ejemplares;

    public Cromo(int num_cromo) {
        this.num_cromo = num_cromo;
        this.ejemplares = 1;
    }

    public void addEjemplar() {
        ejemplares++;
    }

    public void quitarEjemplar() {
        ejemplares--;
    }

    public int getEjemplares() {
        return ejemplares;
    }

    public int getNum_cromo() {
        return num_cromo;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Cromo other = (Cromo) obj;
        if (this.num_cromo != other.num_cromo) {
            return false;
        }
        return true;
    }

}

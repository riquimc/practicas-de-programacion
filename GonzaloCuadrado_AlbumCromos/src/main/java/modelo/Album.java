/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Objects;

/**
 *
 * @author dam1
 */
public class Album {

  private String nombre;
  private int num_cromos;
  private ArrayList<Cromo> cromos;
  private LocalDateTime fecha_creacion;
  private long tiempo_completado;
  private boolean completado;

  public Album(String nombre, int num_cromos) {
    this.nombre = nombre;
    this.num_cromos = num_cromos;
    cromos = new ArrayList<Cromo>(num_cromos);
    fecha_creacion = LocalDateTime.now();
    completado = false;
    tiempo_completado = 0;
  }

  public String getNombre() {
    return nombre;
  }

  public LocalDateTime getFecha_creacion() {
    return fecha_creacion;
  }

  public long getTiempo_completado() {
    return tiempo_completado;
  }

  public boolean isVacio() {
    boolean vacio = false;
    if (this.cromos.isEmpty()) {
      vacio = true;
    }
    return vacio;
  }

  public boolean hayCromoOfrecido(Cromo cromo) {
    boolean hay_cromo = true;
    if (!cromos.contains(cromo) || cromos.get(cromos.indexOf(cromo)).getEjemplares() <= 1) {
      hay_cromo = false;
    }
    return hay_cromo;
  }

  public boolean isCompletado() {
    return completado;
  }

  public int getNum_cromos() {
    return num_cromos;
  }

  public void addCromo(Cromo cromo) {
    if (cromos.size() < this.num_cromos) {
      if (!cromos.contains(cromo)) {
	cromos.add(cromo);
      } else {
	cromos.get(cromos.indexOf(cromo)).addEjemplar();
      }
    } else {
      System.out.println("Ya has completado este álbum, no malgastes el dinero.");
    }
  }

  public void comprobarSiAcabado() {
    boolean terminado = false;
    if (cromos.size() == num_cromos) {
      if (!this.completado) {
	this.tiempo_completado = Duration.between(fecha_creacion, LocalDateTime.now()).getSeconds();
      }
      this.completado = true;

    }
  }

  public boolean hayCromoRepetido(Cromo cromo, Cromo cromo_ofrecido) {
    boolean hay_cromo = false;
    if (cromos.contains(cromo) && cromos.get(cromos.indexOf(cromo)).getEjemplares() > 1 && !cromos.contains(cromo_ofrecido)) {
      hay_cromo = true;
    }
    return hay_cromo;
  }

  public void darCromo(Cromo cromo) {
   // Cromo cromo_elegido = cromos.get(cromos.indexOf(cromo));
	  
    cromo.quitarEjemplar();
    if (cromo.getEjemplares() == 0) {
      cromos.remove(cromos.indexOf(cromo));
    }
  }

  public void recibirCromo(Cromo cromo) {
    cromos.add(cromo);
  }

  public void imprimirCromos() {
    ordenarCromos();
    for (Cromo e : cromos) {
      System.out.println(cromos.indexOf(e) + ". ID: " + e.getNum_cromo() + " Ejemplares: " + e.getEjemplares());
    }
    System.out.println("Número total de cromos que tiene el álbum: " + this.num_cromos);
  }

  private void ordenarCromos() {
    Collections.sort(cromos, new Comparator<Cromo>() {
      public int compare(Cromo o1, Cromo o2) {
	if (o1.getNum_cromo() == o2.getNum_cromo()) {
	  return 0;
	}
	return o1.getNum_cromo() < o2.getNum_cromo() ? -1 : 1;
      }
    });
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    final Album other = (Album) obj;
    if (!Objects.equals(this.nombre, other.nombre)) {
      return false;
    }
    return true;
  }

public ArrayList<Cromo> getCromos() {
	return cromos;
}

  
}

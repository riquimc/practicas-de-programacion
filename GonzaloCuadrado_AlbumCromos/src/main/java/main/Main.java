/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package main;

import java.util.Scanner;
import servicios.Parque_jeringuillas;

/**
 *
 * @author dam1
 */
public class Main {
    public static void main(String[] args) {
        Scanner sc=new Scanner (System.in);
        Parque_jeringuillas p=new Parque_jeringuillas();
        int opcion=0;
        do{
            System.out.println("¿Qué quieres hacer?"
                    + "\n1. Dar de alta un niño"
                    + "\n2. Dar de alta un álbum"
                    + "\n3. Comprar un sobre de cartas"
                    + "\n4. Cambiar cromos"
                    + "\n5. Ver álbumes acabados y tiempo");
            opcion=sc.nextInt();
	    sc.nextLine();
            switch(opcion){
                case 1:
                    p.darAltaNiñato(sc);
                    break;
                case 2:
                    p.darAltaAlbum(sc);
                    break;
                case 3:
                    p.abrirSobre(sc);
                    break;
                case 4:
                    p.cambiarCromos(sc);
                    break;
                case 5:
                    p.verAlbumesCompletados(sc);
                    break;
            }
        }while(opcion!=-1);
    }
}

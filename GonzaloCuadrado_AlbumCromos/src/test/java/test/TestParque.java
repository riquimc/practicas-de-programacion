package test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import modelo.Album;
import modelo.Cromo;
import modelo.Niñato;
import servicios.Parque_jeringuillas;

public class TestParque {
	private static Parque_jeringuillas p;

	@BeforeAll
	static void parque() {
		p = new Parque_jeringuillas();
	}

	@Test
	void addAlumno() {
		Niñato n = new Niñato("Papu");

		boolean niñoEsta = p.darAltaNiñato(n);
		assertEquals(p.getNiñatos().contains(n), niñoEsta);

		Niñato n1 = new Niñato("Lucas");

		boolean niñoEsta2 = p.darAltaNiñato(n1);
		assertNotEquals(p.getNiñatos().contains(n1), niñoEsta2);

	}

	@Test
	void addAlbum() {

		String nombre = "Lol";
		int num_pag = 11;
		Niñato a = new Niñato("Lucas");

		boolean estar = p.darAltaAlbum(nombre, num_pag, a);

		Album album = new Album(nombre, num_pag);

		boolean albumEstar = false;
		if (p.getNiñatos().get(p.getNiñatos().indexOf(a)).getAlbumes().contains(album)) {
			albumEstar = true;
		}

		assertEquals(estar, albumEstar);
	}

	@Test
	void abrirSobre() {
		Niñato a = new Niñato("Lopez");
		Album al = new Album("Pokemon", 9);
		ArrayList<Cromo> cromosUsados = new ArrayList<Cromo>();

		a = p.getNiñatos().get(p.getNiñatos().indexOf(a));
		al = a.buscarAlbum(al);

		cromosUsados = p.abrirSobre(a, al);
		assertEquals(cromosUsados, al.getCromos());
	}

	@Test
	void cambiarCromos() {
		String nombre = "LOL";
		int num = 9;

		Niñato primerNiño = new Niñato("Jed");
		p.darAltaNiñato(primerNiño);
		p.darAltaAlbum(nombre, num, primerNiño);
		Album albumPrimerNiño = new Album(nombre, num);

		Niñato segundoNiño = new Niñato("Carlos");
		p.darAltaNiñato(segundoNiño);
		p.darAltaAlbum(nombre, num, segundoNiño);
		Album albumSegundoNiño = new Album(nombre, num);

		int index = p.getNiñatos().get(p.getNiñatos().indexOf(primerNiño)).getAlbumes().indexOf(albumPrimerNiño);
		int index2 = p.getNiñatos().get(p.getNiñatos().indexOf(segundoNiño)).getAlbumes().indexOf(albumSegundoNiño);
		Cromo cromoPrimerNiño = null;
		Cromo cromoSegundoNiño = null;
		for (int i = 0; i < 2; i++) {
			cromoPrimerNiño = new Cromo(1);
			p.getNiñatos().get(p.getNiñatos().indexOf(primerNiño)).getAlbumes().get(index).addCromo(cromoPrimerNiño);
			cromoSegundoNiño = new Cromo(2);
			p.getNiñatos().get(p.getNiñatos().indexOf(segundoNiño)).getAlbumes().get(index2).addCromo(cromoSegundoNiño);
		}

		Album albumBuscar = p.getNiñatos().get(p.getNiñatos().indexOf(primerNiño)).buscarAlbum(albumPrimerNiño);
		p.cambiarCromos(albumBuscar, p.getNiñatos().get(p.getNiñatos().indexOf(segundoNiño))
				.buscarAlbum(albumSegundoNiño).getCromos().get(0), albumBuscar.getCromos().get(0));

		assertEquals(p.getNiñatos().get(p.getNiñatos().indexOf(segundoNiño)).buscarAlbum(albumSegundoNiño).getCromos()
				.get(1), cromoPrimerNiño);
		assertEquals(albumBuscar.getCromos().get(1), cromoSegundoNiño);
	}
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package servicios;

import java.util.Scanner;
import modelo.Actividad;
import modelo.Alumno;

/**
 *
 * @author oscar
 */
public class Polideportivo {

    private Alumno[] alumnos;
    private Actividad[] actividades;
    private int numeroAlumnos;

    public Polideportivo() {
        this.alumnos = new Alumno[50];
        this.actividades = new Actividad[11];
        this.numeroAlumnos = 0;

        actividades[0] = new Actividad("aerobic", "l,x,v", "10:10-11:00", 35, 47.60);
        actividades[1] = new Actividad("aerobic", "l,x,v", "11:10-12:00", 35, 47.60);
        actividades[2] = new Actividad("aerobic", "l,x,v", "19:10-20:00", 35, 47.60);
        actividades[3] = new Actividad("aerobic", "m,j", "09:10-10:00", 35, 35.70);
        actividades[4] = new Actividad("aerobic", "m,j", "10:10-11:00", 35, 35.70);
        actividades[5] = new Actividad("aerobic", "m,j", "18:10-19:00", 35, 35.70);
        actividades[6] = new Actividad("artes marciales", "l,x,v", "19:00-20:00", 15, 45.00);
        actividades[7] = new Actividad("artes marciales", "m,j", "18:00-19:00", 15, 34.00);
        actividades[8] = new Actividad("artes marciales", "m,j", "19:00-20:00", 15, 34.00);
        actividades[9] = new Actividad("natacion", "l,m,j", "18:10-19:00", 35, 47.60);
        actividades[10] = new Actividad("natacion", "m,j", "17:10-18:00", 35, 35.70);

    }

    public void darAltaAlumno() {
        // pedir el alumno
        Scanner sc = new Scanner(System.in);
        System.out.print("Nombre a dar de alta: ");
        String n = sc.nextLine();
        System.out.print("Apellido a dar de alta: ");
        String a = sc.nextLine();

        // add al array
        if (numeroAlumnos < alumnos.length) {
            this.darAltaAlumno(new Alumno(n, a));
        } else {
            System.out.println("No hay mas plazas libres");
        }
    }

    private void darAltaAlumno(Alumno a) {
        boolean salir = false;
        for (int i = 0; i < alumnos.length && !salir; i++) {
            if (alumnos[i] == null) {
                alumnos[i] = a;
                salir = true;
            }
        }
        numeroAlumnos++;
    }

    public void generarReciboAlumno() {
        // buscar alumno.
        Alumno alumno = buscarAlumno();
        //recorrer sus actividades
        if (alumno != null) {
            System.out.println("el recibo de " + alumno + " es " + alumno.calcularRecibo());
        } else {
            System.out.println("Este alumno no esta en el sistema");
        }
    }

    public void matricularAlumno() {

        boolean mostrar = false;
        // buscar alumno
        Alumno alumno = buscarAlumno();
        if (alumno != null) {
            Actividad actividad = null;
            Scanner sc = new Scanner(System.in);
            System.out.println("1. buscar por actividad\n2. buscar por horario");
            int opcion = sc.nextInt();
            sc.nextLine();
            if (opcion == 1) {
                System.out.println("Dime el nombre de la actividad:");
                String nombreActividad = sc.nextLine();
                for (int i = 0; i < actividades.length; i++) {
                    if (actividades[i].getTipo().equals(nombreActividad)
                            && actividades[i].getPlazasOcupadas() < actividades[i].getPlazas()) {
                        System.out.println((i + 1) + "" + actividades[i]);
                        mostrar = true;
                    }

                }

                if (mostrar == true) {
                    System.out.println("cual quieres??");
                    int nActividad = sc.nextInt();
                    sc.nextLine();
                    actividad = this.actividades[nActividad];
                } else {
                    System.out.println("No hay actividades con plazas vacantes");
                }
            } else {
                System.out.println("Dime el horario que quieres");
                String horario = sc.nextLine();
                for (int i = 0; i < actividades.length; i++) {
                    if (actividades[i].getHorario().equals(horario)
                            && actividades[i].getPlazasOcupadas() < actividades[i].getPlazas()) {
                        System.out.println((i + 1) + "" + actividades[i]);
                        mostrar = true;
                    }

                }
                if (mostrar == true) {
                    System.out.println("cual quieres??");
                    int nActividad = sc.nextInt();
                    sc.nextLine();
                    actividad = this.actividades[nActividad];
                } else {
                    System.out.println("No hay actividades con plazas vacantes");
                }
            }
            // buscar actividad
            // buscar horario
            //add alumno a actividad.
            if (mostrar) {
                actividad.darAltaAlumno(alumno);
                alumno.addActividad(actividad);
            }
        } else {
            System.out.println("Este alumno no se encuentra en el sistema");
        }
    }

    public void darBajaAlumno() {
        // busca Alumno
        int indice = buscarAlumnoPorIndice();
        if (indice != -1) {
            // darle de baja en array de alumnos
            this.alumnos[indice].desmatricularDeTodasActividades();
            // darle de baja en todas las actividades del alumno.

            this.alumnos[indice] = null;
            int ind = buscarActividad();
            this.actividades[ind].darBajaAlumno(alumnos[indice]);
            numeroAlumnos--;
        } else {
            System.out.println("Este alumno no se encuentra en el sistema");
        }
    }

    public void generarReciboActividad() {
        // buscar actividad
        int indice = buscarActividad();
        Actividad actividad = this.actividades[indice];
        // multiplicar precio por plazas.
        if (actividad.getPlazasOcupadas()>0) {
            System.out.println("el precio de la " + actividad + " es " + actividad.getCoste() * actividad.getPlazasOcupadas());
        }else{
            System.out.println("No hay alumnos matriculados con lo cual no se ha recaudado nada");
        }
    }

    private Alumno buscarAlumno() {
        int indice = buscarAlumnoPorIndice();
        Alumno buscar = null;
        if (indice != -1) {
            buscar = this.alumnos[indice];
        }
        return buscar;
    }

    private int buscarAlumnoPorIndice() {
        int buscar = 0;
        boolean ok = false;
        Scanner sc = new Scanner(System.in);
        System.out.print("Nombre : ");
        String nombre = sc.nextLine();
        System.out.print("Apellido : ");
        String apellidos = sc.nextLine();

        for (int i = 0; i < numeroAlumnos && !ok; i++) {
            if (this.alumnos[i].getNombre().equals(nombre)
                    && this.alumnos[i].getApellido().equals(apellidos)) {
                buscar = i;
                ok = true;
            }
        }
        if (!ok) {
            buscar = -1;
        }
        return buscar;
    }

    private int buscarActividad() {
        int buscar = 0;

        Scanner sc = new Scanner(System.in);
        boolean encontrado = false;
        System.out.println("Que actividad desea?");
        String nombreActividad = sc.nextLine();
        System.out.println("Que dias desea?");
        String diasAct = sc.nextLine();
        System.out.println("Que horario desea?");
        String horAct = sc.nextLine();
        encontrado = false;

        for (int i = 0; i < actividades.length && !encontrado; i++) {
            if (actividades[i].getTipo().equals(nombreActividad) && actividades[i].getDias().equals(diasAct) && actividades[i].getHorario().equals(horAct)) {
                buscar = i;
                encontrado = true;

            }
        }
        return buscar;
    }
}

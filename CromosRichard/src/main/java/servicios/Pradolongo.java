package servicios;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import modelo.Album;
import modelo.Cromo;
import modelo.Usuario;
import utiles.Tipo;

public class Pradolongo {
	Scanner sc = new Scanner(System.in);
	Random r = new Random();
	private ArrayList<Usuario> usuarios;

	public Pradolongo() {
		this.usuarios = new ArrayList<Usuario>();
	}

	public void darAltaUsuario() {
		System.out.println("Dime nombre");
		String nombre = sc.nextLine();

		System.out.println("Dime DNI");
		String dni = sc.nextLine();

		Usuario usuario = new Usuario(nombre, dni);

		if (!usuarios.contains(usuario)) {
			usuarios.add(usuario);
		} else {
			System.out.println("Este usuario ya existe");
		}
	}

	public void darAltaAlbum() {
		Usuario user = buscarUsuario();

		if (user.getNombre()  != null) {
			Tipo tipo = null;

			System.out.println("Nombre del album");
			String nombre = sc.nextLine();
			System.out.println("Tipo de album\nPanini\nAdrenalin\nPokemon");
			String type = sc.nextLine();
			System.out.println("Numero máximo de cromos");
			int nMax = sc.nextInt();
			Album album = new Album(tipo.valueOf(type), nombre, nMax, LocalDateTime.now());

			if (user.devolverAlbum(album).getNCromosMax() == 0) {
				user.setAlbumes(album);
			}else {
				System.out.println("Este album ya existe");
			}
		} else {
			System.out.println("Este usuario no se encuentra en la base de datos");
		}
	}

	public void abrirBlister() {
		Usuario user = buscarUsuario();
		Album album = buscarAlbum(user);
		if (user.getNombre() != null || album.getFechaIn() != null) {
			for (int i = 0; i < 10 && album.nCromos() < album.getNCromosMax(); i++) {
				Cromo c1 = new Cromo(r.nextInt(album.getNCromosMax() + 1));
				album.comprabarCromoRep(c1);
				album.setCromos(c1);
				System.out.println(c1.toString());
			}
			if (album.nCromos() == album.getNCromosMax()) {
				album.setFechaFin(LocalDateTime.now());
				System.out.println("Este album esta completado\n");
			}
		} else {
			System.out.println("Este album o usuario no existen\n");
		}
	}

	public void cambiarCromos() {
		Usuario user = buscarUsuario();
		Album album = buscarAlbum(user);
		if (user.getNombre() != null || album.getFechaIn() != null) {
			album.imprimirRepes();
			System.out.println("¿Qué cromo quieres dar?");
			int i = sc.nextInt();

			Cromo c = album.getCromos(i);

			album.imprimirNole(album.getNCromosMax());
			System.out.println("¿Qué cromo quieres?");
			int b = sc.nextInt();

			Album a;
			boolean salir = false;
			for (int j = 0; j < usuarios.size() && !salir; j++) {
				a = usuarios.get(j).devolverAlbumTipo(album);
				// recorrernos el album a ver si tiene el cromo que queremos repe y si no tiene
				// el que le queremos dar
				if (a.buscarCromosCambio(b, c).getIdCromo() != 0) {
					a.setCromos(a.buscarCromosCambio(b, c));
					salir = true;
				}
			}
			if (!salir) {
				System.out.println("No se ha ralizado el cambio de cromos\n");
			}
		}
	}

	public void mostrarAlbumAcabado() {
		Usuario user = buscarUsuario();
		if (user.getNombre() != null) {
			user.encontrarAlbumAcabado();
		} else {
			System.out.println("Este usuario no existe\n");
		}
	}

	private Usuario buscarUsuario() {
		System.out.println("Dime nombre");
		String nombre = sc.nextLine();
		sc.nextLine();
		System.out.println("Dime DNI");
		String dni = sc.nextLine();

		Usuario user = new Usuario(nombre, dni);

		if (usuarios.contains(user)) {
			int indice = usuarios.indexOf(user);
			user = usuarios.get(indice);
		} else {
			user = new Usuario(null, null);
		}
		return user;
	}

	private Album buscarAlbum(Usuario u) {
		Tipo tipo = null;

		System.out.println("Nombre del album");
		String nombre = sc.nextLine();
		System.out.println("Tipo de album\nPanini\nAdrenalin\nPokemon");
		String type = sc.nextLine();
		System.out.println("Numero máximo de cromos");
		int nMax = sc.nextInt();
		Album album = new Album(tipo.valueOf(type), nombre, nMax, null);

		album = u.devolverAlbum(album);

		return album;
	}

}

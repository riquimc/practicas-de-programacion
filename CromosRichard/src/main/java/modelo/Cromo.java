package modelo;

public class Cromo {
	private int idCromo;
	private int nRepes;

	@Override
	public String toString() {
		return "Cromo [idCromo=" + this.idCromo + "]";
	}

	public Cromo(int idCromo) {
		this.idCromo = idCromo;
	}

	public int getnRepes() {
		return nRepes;
	}

	public void setnRepes(int nRepes) {
		this.nRepes = nRepes;
	}

	public int getIdCromo() {
		return idCromo;
	}

}

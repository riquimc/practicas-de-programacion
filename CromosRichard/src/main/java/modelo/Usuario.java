package modelo;

import java.time.Duration;
import java.util.ArrayList;

public class Usuario {
	private String dni;
	private String nombre;
	private ArrayList<Album> albumes;

	public Usuario(String dni, String nombre) {
		this.dni = dni;
		this.nombre = nombre;
		this.albumes = new ArrayList<Album>();
	}

	public Album devolverAlbum(Album a) {
		if (albumes.contains(a)) {
			int indice = albumes.indexOf(a);
			a = albumes.get(indice);
		} else {
			a = new Album(null,null,0,null);
		}
		return a;
	}

	public void encontrarAlbumAcabado() {
		for (Album a : albumes) {
			if (a.getFechaFin() != null) {
				System.out.println(a.toString() + "está acabado");
				Duration d = Duration.between(a.getFechaIn(), a.getFechaFin());
				System.out.println("Se ha tardado en completarlo " + d);
			}
		}
	}

	public Album devolverAlbumTipo(Album a) {
		for (Album al : albumes) {
			if (al.getTipo().equals(a.getTipo())) {
				a = al;
			}
		}
		return a;

	}

	public void setAlbumes(Album albumes) {
		this.albumes.add(albumes);
	}

	public String getDni() {
		return dni;
	}

	public String getNombre() {
		return nombre;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dni == null) ? 0 : dni.hashCode());
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (dni == null) {
			if (other.dni != null)
				return false;
		} else if (!dni.equals(other.dni))
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		return true;
	}

}

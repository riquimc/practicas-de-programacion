package modelo;

import java.time.LocalDateTime;
import java.util.ArrayList;

import utiles.Tipo;

public class Album {
	private ArrayList<Cromo> cromos;
	private Tipo tipo;
	private String nombre;
	private int nCromosMax;
	private LocalDateTime fechaIn;
	private LocalDateTime fechaFin;

	public Album(Tipo tipo, String nombre, int nCromosMax, LocalDateTime fechaIn) {
		this.tipo = tipo;
		this.nombre = nombre;
		this.nCromosMax = nCromosMax;
		this.fechaIn = fechaIn;
		cromos = new ArrayList<Cromo>();
	}

	public Cromo getCromos(int i) {
		return cromos.get(i);
	}

	public void setCromos(Cromo cromos) {
		this.cromos.add(cromos);
	}

	public Tipo getTipo() {
		return tipo;
	}

	public void comprabarCromoRep(Cromo c) {
		if (cromos.contains(c)) {
			c.setnRepes(c.getnRepes() + 1);
		}
	}

	public void imprimirRepes() {
		int i = 1;
		for (Cromo c : cromos) {
			if (c.getnRepes() > 0) {
				System.out.println(i + ". " + c.toString());
			}
			i++;
		}
	}

	public void imprimirNole(int nCromosDisponible) {

		for (int i = 1; i < nCromosDisponible; i++) {
			for (Cromo c : cromos) {
				if (c.getIdCromo() != i) {
					System.out.println(i);
				}
			}
		}
	}

	public Cromo buscarCromosCambio(int i, Cromo c) {
		boolean salir = false;
		for (int j = 0; j < cromos.size() && !salir; j++) {
			if (cromos.get(j).getIdCromo() == i && cromos.get(j).getnRepes() > 0 && !cromos.contains(c)) {
				cromos.add(new Cromo(c.getIdCromo()));
				c = cromos.get(j);
				salir = true;
			}
		}
		if(!salir) {
			c=new Cromo(0);
		}
		return c;
	}

	public String getNombre() {
		return nombre;
	}

	public int getNCromosMax() {
		return nCromosMax;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + nCromosMax;
		result = prime * result + ((nombre == null) ? 0 : nombre.hashCode());
		result = prime * result + ((tipo == null) ? 0 : tipo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Album other = (Album) obj;
		if (nCromosMax != other.nCromosMax)
			return false;
		if (nombre == null) {
			if (other.nombre != null)
				return false;
		} else if (!nombre.equals(other.nombre))
			return false;
		if (tipo != other.tipo)
			return false;
		return true;
	}

	public LocalDateTime getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(LocalDateTime fechaFin) {
		this.fechaFin = fechaFin;
	}

	public LocalDateTime getFechaIn() {
		return fechaIn;
	}

	public int nCromos() {
		return cromos.size();
	} 

	@Override
	public String toString() {
		return "Album [tipo=" + tipo + ", nombre=" + nombre + "]";
	}
}

package main;

import java.util.Scanner;

import servicios.Pradolongo;

/**
 * Hello world!
 *
 */
public class Main {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Pradolongo parque = new Pradolongo();
		boolean salir = false;

		do {
			System.out.println("Bienvenido al programa de gestion de cromos");
			System.out.println("¿Qué desea hacer?" + "\n1.Dar de alta usuario" + "\n2.Dar de alta album"
					+ "\n3.Abrir Blister de 10 cromos" + "\n4.Cambiar cromos" + "\n5.Mostrar albumes terminados"
					+ "\n6.Salir");
			int opcion = sc.nextInt();
			switch (opcion) {
			case 1:
				parque.darAltaUsuario();
				break;
			case 2:
				parque.darAltaAlbum();
				break;
			case 3:
				parque.abrirBlister();
				break;
			case 4:
				parque.cambiarCromos();
				break;
			case 5:
				parque.mostrarAlbumAcabado();
				break;
			case 6:
				salir = true;
				break;
			default:
				break;

			}
		} while (!salir);
	}
}
